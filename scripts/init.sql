drop table if exists product;

CREATE TABLE product (
     id  int NOT NULL AUTO_INCREMENT ,
     name varchar(60) NOT NULL,
     description varchar(60) NOT NULL,
     price int NOT NULL DEFAULT 0,
     category varchar(20) NOT NULL,
     CONSTRAINT product_pk PRIMARY KEY (id)
);

insert into product(name, description, price, category)
values ('iphone13', 'apple iphone 13 product', 1300000, 'phone');